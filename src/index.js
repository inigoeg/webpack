import { DateTime } from 'luxon';

import "./css/reset.css";
import "./scss/style.scss";

import reloj from "./images/reloj.svg";

let relojElement = 
    document.getElementById("reloj");
relojElement.src = reloj;

const printDateTime = () => 
{
    const fechaActual = 
        DateTime.now()
        .setZone('Europe/Madrid')
        .setLocale('es-ES');

    let dateElement = 
        document.getElementById("date");
    dateElement.innerText = 
        fechaActual.toLocaleString();

    let timeElement = 
        document.getElementById("time");
    timeElement.innerText = 
        fechaActual.toLocaleString(DateTime.TIME_WITH_SECONDS);
}

printDateTime();
setInterval(() => printDateTime(), 1000);