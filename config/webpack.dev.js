const common = require("./webpack.common");
const { merge } = require("webpack-merge");

const devConfig = {
    mode: "development",
    devtool: "eval-source-map",
    devServer: {
        port: 3000,
        watchFiles: "../dist",
        open: "/"
    }
}

module.exports = merge(common, devConfig);
