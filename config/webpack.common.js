const path = require("path");
const { CleanPlugin } = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

/** @type {import('webpack').Configuration} */
module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "../dist"),
        filename: 'main.[contenthash].js'
    },
    module: { 
        rules: [
            {
                use: [MiniCssExtractPlugin.loader,"css-loader","sass-loader"],
                test: /.(css|sass|scss)$/
            },
            {
                type: "asset",
                test: /\.(png|svg|jpg|jpeg|gif)$/i
            }
        ]
    },
    plugins : [
        new CleanPlugin(),
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        }),
        new MiniCssExtractPlugin()
    ]
};